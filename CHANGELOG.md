# Changelog

## 2021-04-12 (v 0.7.2)

Initial public release

## 2021-09-13 (v 0.7.4)

Er is vooral gewerkt aan de instantiemodellen in het kader van regelpublicatie, diensten en koppelvlakken. Bedenk dat deze modellen onder handen werk zijn en nog aan significante veranderingen onderhevig kunnen zijn.
Daarentegen, de ruggengraat - namelijk de profielgegevens voor de nodige inkomensdiensten - is redelijk stabiel. Het behoeft hier en daar bijwerkingen.

Voor de verschillende modellen zijn we bezig met beschrijvingen van de overwegingen, requirements, ontwerpbeslissingen etc... Die moeten uiteindelijk ook worden opgenomen bij de modelpublicaties in de ontologie.